package com.kodpoint.repository;

import com.kodpoint.domain.Users;
import org.apache.catalina.User;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Alkan on 2.03.2017.
 */

@Transactional
public interface UsersRepository extends CrudRepository<Users,Long> {

    @Query("SELECT u FROM Users u where u.id = 122575")
    List<Users> oneSes();


}
