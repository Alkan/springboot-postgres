package com.kodpoint;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by Alkan on 29.05.2017.
 */

@Component
public class RedisCacheToJson {

    @Autowired
    private StringRedisTemplate template;

    public <T> T cachable(T oe, String key) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ValueOperations<String, String> ops = this.template.opsForValue();

        if (!this.template.hasKey(key)) {
            ops.set(key,mapper.writeValueAsString(oe));
        }
        else{
            return mapper.readValue(ops.get(key),  new TypeReference<T>() {});
        }
        return null;
    }
}
