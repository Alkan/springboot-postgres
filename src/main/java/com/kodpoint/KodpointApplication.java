package com.kodpoint;


import com.kodpoint.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class KodpointApplication  implements CommandLineRunner {

	@Autowired
	UsersRepository usersRepository;

	@Autowired
	RedisCacheToJson redisCacheToJson;

	public static void main(String[] args) {
		SpringApplication.run(KodpointApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {

	System.out.println(redisCacheToJson.cachable(usersRepository.oneSes(),"user:top").size());
	}



}
