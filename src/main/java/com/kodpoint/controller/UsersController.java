package com.kodpoint.controller;

import com.kodpoint.domain.Users;
import com.kodpoint.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Alkan on 2.03.2017.
 */

@RestController
@RequestMapping("user")
public class UsersController {

    @Autowired
    UsersRepository usersRepository;

    @RequestMapping(path = "/insert", method = RequestMethod.POST)
    public Users insertUser(@RequestBody Users users){
        usersRepository.save(users);
        return users;

    }

    @RequestMapping(path = "/select", method = RequestMethod.GET)
    public List<Users> select(){
        List<Users> users =    usersRepository.oneSes();
        return users;

    }

}
