package com.kodpoint.domain;




import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Alkan on 2.03.2017.
 */
@Entity
@Table(name = "users")
public class Users implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter@Setter
    private Long id;



    public Users(String username, String email) {
        this.username = username;
        this.email = email;
    }

    @Getter@Setter
    @NotNull
    private String username;
    @Getter@Setter
    @NotNull
    private String email;

    public Users() {
    }



}
